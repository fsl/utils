/*! \file threading.h
    \brief Contains declaration of classes/functions useful for multi-threading

    \author Jesper Andersson
    \version 1.0b, Feb., 2022.
*/
//
// threading.h
//
// Jesper Andersson, FMRIB Image Analysis Group
//
// Copyright (C) 2020 University of Oxford
//

#ifndef threading_h
#define threading_h

#include <cmath>
#include <cstdint>
#include <thread>
#include <vector>


namespace Utilities {

/****************************************************************//**
*
* \brief This is a class that is used to specify number of threads.
*
* This is a class that is used instead of an int to to specify number
* of threads for functions and/or constructors. It is intended to
* allow for a default value without risk of any ambiguity in the
* function signature.
*
* When a NoOfThreads instance is created:
*
*  - If a positive value is passed in, it is used unchanged.
*
*  - If a zero or negative value is passed in, the value of the
*    ${FSL_NUM_THREADS} environment variable is used.
*
*  - If the ${FSL_NUM_THREADS} environment variable is not set, or
*    is set to an invalid value, 1 is used.
*
********************************************************************/
struct NoOfThreads
{
  explicit NoOfThreads(int64_t nthr=0);
  ~NoOfThreads();
  // n and _n are equivalent
  const int64_t _n;
  const int64_t n;

// These fields are used to set a default value
// for nthr for when a value of <= 0 is provided
private:
  static const int64_t default_num_threads;
  static int64_t       determine_num_threads(int64_t n);
};


/**
 * This function can be used to initialise multi-threading for BLAS
 * operations. It is intended to be called at/near the beginning of
 * the main() function of an executable, and must be called before
 * any BLAS routines are invoked.
 *
 * The behaviour of this function depends on which BLAS implementation
 * is in use. Currently, if OpenBLAS is being used, threading is
 * disabled, as the OpenBLAS threading pool can often have a negative
 * effect on performance.
 *
 */
void fsl_init_blas_threading(uint16_t flags=0);


/**
 * The following code provides two templated functions for performing
 * tasks in parallel. These functions perform the same task as std::for_each
 * and std::transform, but allow explicit control over the number of threads.
 *
 *  - parfor: - equivalent of std::for_each
 *  - apply:  - equivalent of std::transform
 *
 * The logic for both of these functions is defined in the _parfor function,
 * and _call sub-functions.
 */


// Called by _parfor when the function output
// is to be written to the output vector (i.e.
// write_output == true)
template<bool         write_output,
         typename     Func,
         typename     Ot,
         typename     It,
         typename ... Its>
inline typename std::enable_if<write_output>::type
_call(Func          func,
      Ot            output,
      const It      input,
      const Its ... inputs) {
  (*output) = func(*input, *inputs ...);
}

// Called by _parfor when the function output
// is to be ignored (i.e. write_output == false)
template<bool         write_output,
         typename     Func,
         typename     Ot,
         typename     It,
         typename ... Its>
inline typename std::enable_if<!write_output>::type
_call(Func          func,
      Ot            output,
      const It      input,
      const Its ... inputs) {
  func(*input, *inputs ...);
}

// Called by parfor and apply - handles both
// cases where the function output is to be
// written to the output vector (apply), and
// where it is to be ignored (parfor)
template<bool         write_output,
         typename     Func,
         typename     Ot,
         typename     It,
         typename ... Its>
void _parfor(NoOfThreads   nthr,
             Func          func,
             Ot            output,
             const It      infirst,
             const It      inlast,
             const Its ... inputs) {

  size_t nelems        = (inlast - infirst);
  size_t elems_per_thr = std::ceil(nelems / static_cast<float>(nthr.n));

  std::vector<std::thread> threads;

  auto run_block = [&](size_t thri) {
    size_t start = thri * elems_per_thr;
    size_t end   = std::min(start + elems_per_thr, nelems);

    for (auto i = start; i < end; i++) {
      _call<write_output, Func, Ot, Its ...>(
        func, (output + i), infirst + i, (inputs + i) ...);
    }
  };

  // run (nthr-1) blocks on separate threads
  for (auto thri = 1; thri < nthr.n; thri++) {
    threads.emplace_back(run_block, thri);
  }
  // run first block on calling thread
  run_block(0);

  for (auto& t : threads) {
    t.join();
  }
}

/**
 * parfor - apply a function to each element in a vector in parallel.
 *
 * This function is essentially does the same as std::for_each,
 * but allows explicit control over the number of threads.
 *
 * The It and Its argements must be random access iterators with the
 * same number of elements.
 *
 * Func must be callable with elements from inputs
 *
 * Example contrived usage:
 *
 *     void run_task(int value) {
 *         std::cout << value << std::endl;
 *     }
 *
 *     std::vector<int> values = {1, 2, 3, 4, 5};
 *     Utilities::NoOfThreads nthr(0);
 *     Utilities::parfor(nthr, run_task, values.begin(), values.end());
 *
 * The parfor function can be called in one of the following ways to
 * control the number of threads:
 *
 *   - With a Utilities::NoOfThreads instance:
 *
 *         parfor(NoOfThreads(16), func, ...);
 *
 *   - With an integer:
 *
 *         parfor(16, func, ...);
 *
 *   - Without specifying the number of threads (FSL_NUM_THREADS will
 *     be used, or 1 if FSL_NUM_THREADS is unset):
 *
 *         parfor(func, ...);
 */
template<typename     Func,
         typename     It,
         typename ... Its>
void parfor(NoOfThreads   nthr,
            Func          func,
            const It      infirst,
            const It      inlast,
            const Its ... inputs) {

  // We still need to pass an output
  // iterator, but it is not used, so
  // doesn't need to be valid.
  char* v;
  _parfor<false, Func, decltype(v), It, Its ...>(
    nthr, func, v, infirst, inlast, inputs ...);
}

/**
 * Variant of parfor() which accepts an integer instead of
 * Utilities::NoOfThreads.
 */
template<typename     Func,
         typename     It,
         typename ... Its>
void parfor(int           nthr,
            Func          func,
            const It      infirst,
            const It      inlast,
            const Its ... inputs) {
  char* v;
  _parfor<false, Func, decltype(v), It, Its ...>(
    NoOfThreads(nthr), func, v, infirst, inlast, inputs ...);
}


/**
 * Variant of parfor() which uses a default value for Utilities::NoOfThreads.
 */
template<typename     Func,
         typename     It,
         typename ... Its>
// enable_if required to prevent this from matching the other templates
typename std::enable_if<!(std::is_same<Func, NoOfThreads>::value ||
                          std::is_same<Func, int>        ::value)>::type
parfor(Func          func,
       const It      infirst,
       const It      inlast,
       const Its ... inputs) {
  char* v;
  _parfor<false, Func, decltype(v), It, Its ...>(
    NoOfThreads(), func, v, infirst, inlast, inputs ...);
}


/**
 * Apply a function to each element in a vector in parallel, storing
 * the result in an output vector.
 *
 * This function is essentially does the same as std::transform,
 * but allows explicit control over the number of threads.
 *
 * The It, Its, and Ot argements must be random access iterators with
 * the same number of elements.
 *
 * Func must be callable with elements from inputs, and produce a
 * value that can be inserted into outputs.
 *
 * Example usage:
 *
 *     int mul(int value1, int value2) {
 *         return value1 * value2;
 *     }
 *
 *     std::vector<int> values1 = {1, 2, 3, 4, 5};
 *     std::vector<int> values2 = {1, 2, 3, 4, 5};
 *     std::vector<int> result(values1.size());
 *
 *     Utilities::NoOfThreads nthr(0);
 *     Utilities::apply(nthr, mul,
 *                      result.begin(),
 *                      values1.begin(), values1.end(),
 *                      values2.begin());
 *
 * The apply function can be called in one of the following ways to
 * control the number of threads:
 *
 *   - With a Utilities::NoOfThreads instance:
 *
 *         apply(NoOfThreads(16), func, ...);
 *
 *   - With an integer:
 *
 *         apply(16, func, ...);
 *
 *   - Without specifying the number of threads (FSL_NUM_THREADS will
 *     be used, or 1 if FSL_NUM_THREADS is unset):
 *
 *         apply(func, ...);
 */
template<typename     Func,
         typename     Ot,
         typename     It,
         typename ... Its>
void apply(NoOfThreads   nthr,
           Func          func,
           Ot            output,
           const It      infirst,
           const It      inlast,
           const Its ... inputs) {
  _parfor<true, Func, Ot, It, Its ...>(
    nthr, func, output, infirst, inlast, inputs ...);
}


/**
 * Variant of apply() which accepts an integer instead of
 * Utilities::NoOfThreads.
 */
template<typename     Func,
         typename     Ot,
         typename     It,
         typename ... Its>
void apply(int           nthr,
           Func          func,
           Ot            output,
           const It      infirst,
           const It      inlast,
           const Its ... inputs) {
  _parfor<true, Func, Ot, It, Its ...>(
    NoOfThreads(nthr), func, output, infirst, inlast, inputs ...);
}

/**
 * Variant of apply() which uses a default value for Utilities::NoOfThreads.
 */
template<typename     Func,
         typename     Ot,
         typename     It,
         typename ... Its>
// enable_if required to prevent this from matching the other templates
typename std::enable_if<!(std::is_same<Func, NoOfThreads>::value ||
                          std::is_same<Func, int>        ::value)>::type
apply(Func          func,
      Ot            output,
      const It      infirst,
      const It      inlast,
      const Its ... inputs) {
  _parfor<true, Func, Ot, It, Its ...>(
    NoOfThreads(), func, output, infirst, inlast, inputs ...);
}

} // End namespace Utilities

#endif // End #ifndef threading_h
